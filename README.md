
# Clase 'Particulas' para la simulación del movimiento de partículas cargadas 

  

Esta clase, '**Particulas**', permite la simulación del movimiento de partículas cargadas debido a la influencia del campo magnético y del campo eléctrico. Esta clases incluye métodos para calcular la fuerza de Lorentz y de Coulomb, para actualizar la posición,velocidad y aceleración de la partícula.

## Clase 'Particulas'

La clase **'Particulas'** representa partículas en el sistema físico y tiene los siguientes atributos:

* **'masa'**: Masa de la partícula.
*  **'carga'**: Carga de la partícula.
*  **'posicion'**: Posición tridimensional de la partícula.
* **'velocidad'**: Velocidad tridimensional de la partícula
* **'aceleracion'**: Aceleración tridimensional de la partícula.

### Métodos

 1.  **`__init__(self, _masa, _carga, _posicion, _velocidad, _aceleracion)`**
    Inicializa una partícula con valores dados para masa, carga, posición, velocidad y aceleración.
    
2.  **`lorentz(self, campo_magnetico)`**
    
    Calcula la fuerza de Lorentz experimentada por la partícula debido al campo magnético.
    
3.  **`coulomb(self, particula2)`**
    
    Calcula la fuerza de Coulomb entre la partícula y otra partícula dada.
    
4.  **`movimiento_particula(self, tiempo, campo_magnetico, particula2)`**
    
    Simula el movimiento de la partícula en función del tiempo bajo la influencia de fuerzas magnéticas y eléctricas.
## Librerías requeridas

Para hacer uso correcto de este código, es necesario tener instaladas las librerías:

```python
numpy 
matplotlib
celluloid
IPython
```

Puede instalarlas haciendo uso del comando:

```python
pip install matplotlib  # ejemplo de instalación
```
  
  ##  Uso
Para utilizar este código, simplemente crea instancias de la clase `Particulas`, establece los parámetros iniciales y utiliza el método `movimiento_particula` para simular el movimiento en el tiempo.


```python
import Particulas
# Ejemplo de uso
particula1 = Particulas(masa=1.0, carga=1.0, posicion=[0.0, 0.0, 0.0], velocidad=[1.0, 0.0, 0.0], aceleracion=[0.0, 0.0, 0.0])
particula2 = Particulas(masa=1.0, carga=-1.0, posicion=[-1.0, 0.0, 0.0], velocidad=[-1.0, 0.0, 0.0], aceleracion=[0.0, 0.0, 0.0])
```
Luego define el campo magnetico y el paso del tiempo para la simulación.
```python
campo_magnetico = [0,0,1]
tiempo = 0.01
```
Por ultimo, llama el metodo `movimiento_particula`para cada particula, introduciendo el campo magnetico, el paso del tiempo y los argumentos de la otra particula.

```python
aceleracion1, velocidad1, posicion1 = particula1.movimiento_particula(tiempo, campo_magnetico, particula2)
aceleracion2, velocidad2, posicion2 = particula2.movimiento_particula(tiempo, campo_magnetico, particula1)
``` 
Esto actualizara la posición, velocidad, y aceleración de cada partícula de acuerdo a la fuerza de Lorentz y de Coulomb.




## Contacto

Para cualquier pregunta o comentario, no dudes en ponerte en contacto con los autores:

-   **Andres Felipe Riaño Quintanilla**
    
    -   Email: andres.rianoq@udea.edu.co
-   **Julian Francisco Pinchao Ortiz**
    
    -   Email: jfrancisco.pinchao@udea.edu.co
-   **Santiago Julio Davila**
    
    -   Email: santiago.juliod@udea.edu.co





