import numpy as np 
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

class Particulas:
    
    masa = 0
    carga = 0
    posicion = np.zeros(3) 
    velocidad = np.zeros(3)
    aceleracion = np.zeros(3)
    
    def __init__(self,_masa,_carga,_posicion,_velocidad,_aceleracion):
        '''Asigna valores a las particulas de la clase: 
        params:
        _masa: valor de la masa de la particula [g]
        _carga: valor de la carga de la particula [statC]
        _posicion: arreglo de 3 entradas correspondiente a la posición inicial de la particula [cm]
        _velocidad: arreglo de 3 entradas correspondiente a la velocidad inicial de la particula [(cm/s)/c]
        _aceleracion: arreglo de 3 entradas correspondiente a la aceleración inicial de la particula (por defecto igual a cero) [cm/s²]
        
        ADVERTENCIA: es necesario importar numpy antes de instanciar esta clase.
        '''
        self.masa = _masa
        self.carga = _carga
        self.posicion = _posicion
        self.velocidad = _velocidad
        self.aceleracion = _aceleracion

        
    def lorentz(self,campo_magnetico):
        
        '''
        Calcula la fuerza de Lorentz sobre la partícula cargada debido a 
        su movimiento en un campo magnético.

        INPUT:
        campo_magnetico: arreglo de tres entradas correspondiente al campo magnético [G] (array)
        
        OUTPUT:
        F_L: arreglo de tres entradas correspondiente a la fuerza magnética [dyn] (array)
        '''

        return np.cross(self.velocidad,self.carga*campo_magnetico)
    
    def coulomb(self, particula2):
        
        '''
        Calcula la fuerza electrostática sobre la partícula cargada debido a la influencia
        de otra partícula cargada.

        INPUT:
        q2: carga de la segunda partícula [statC] (float)
        r2: arreglo de tres entradas correpsondiente a la posición de la segunda partícula [cm] (array)
        
        OUTPUT:
        F_C: arreglo de tres entradas correspondiente a la fuerza magnética [dyn] (array)
        '''

        r= particula2.posicion - self.posicion
        distancia= np.linalg.norm(r)
        magnitud=(self.carga * particula2.carga)/distancia**2
        direccion=-r/distancia

        return magnitud*direccion
    
    def movimiento_particula(self,tiempo,campo_magnetico,particula2):
        '''
        Calcula la posición, velocidad y aceleración de una partícula dadas las fuerzas entre ellas.
        Modifica los atributos de la instancia a estos valores calculados.

        INPUT:
        fuerzas: arreglo que contiene varios arreglos de 3 entradas correspondientes a 
                 las fuerzas sobre la partícula [newtons] (array)
        OUTPUT:
        posición [cm]
        velocidad [(cm/s)/c]
        aceleración: [cm/s²]
        '''

        fuerza_coulomb = self.coulomb(particula2)
        fuerza_lorentz = self.lorentz(campo_magnetico)

        fuerza_total= fuerza_lorentz + fuerza_coulomb
        self.aceleracion = fuerza_total / self.masa 

        self.velocidad = self.aceleracion*tiempo + self.velocidad
        self.posicion  = self.velocidad*tiempo + 0.5*self.aceleracion*(tiempo)**2 + self.posicion
        return self.aceleracion,self.velocidad,self.posicion



        
